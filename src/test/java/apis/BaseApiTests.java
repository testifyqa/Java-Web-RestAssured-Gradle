package apis;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.http.ContentType;
import static io.restassured.path.json.JsonPath.from;
import org.testng.Assert;

import static io.restassured.RestAssured.given;
import static utils.settings.Settings.*;

public class BaseApiTests {

    private static Response response;
    private static RequestSpecification httpRequest;
    private static String strResponse;

    public static void setBaseUri() {
        RestAssured.baseURI = baseUrl;
    }

    public static RequestSpecification authTwitter() {
        httpRequest = given().auth().oauth(consumerKey, consumerSecret, accessToken, accessTokenSecret);
        return httpRequest;
    }

    public static void postTweet(String message) {
        response = authTwitter().queryParam("status", message)
                .post("/update.json");
    }

    public static void getRequest(String apiResource) {
        response = authTwitter().get(apiResource);
        response.then().assertThat().statusCode(200)
                .and().contentType(ContentType.JSON);
        strResponse = response.asString();
    }

    public static void assertKeyValue(String keyValue, String passedValue) {
        keyValue = from(strResponse).get(keyValue);
        Assert.assertEquals(keyValue, passedValue);
    }
}
