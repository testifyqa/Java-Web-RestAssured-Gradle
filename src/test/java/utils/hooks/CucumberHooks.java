package utils.hooks;

import apis.BaseApiTests;
import io.cucumber.java.Before;

public class CucumberHooks {

    @Before("@Api")
    public void setBaseUri() {
        BaseApiTests.setBaseUri();
    }
}